import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import Routes from './Routes';
import reducers from './reducers';

class App extends Component {

    componentWillMount() {

        firebase.initializeApp({
            apiKey: "AIzaSyA4ooicZH1GqhRCFA3kXm_NGANp947Ez8w",
            authDomain: "whatsapp-clone-9dc68.firebaseapp.com",
            databaseURL: "https://whatsapp-clone-9dc68.firebaseio.com",
            projectId: "whatsapp-clone-9dc68",
            storageBucket: "whatsapp-clone-9dc68.appspot.com",
            messagingSenderId: "295872899009"
        });
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
                <Routes />
            </Provider>
        );
    }
}

export default App;
